/// WMF kafka sources for timely dataflow
/// This works, but is experimental
use std::time::{Instant, Duration};

use futures_util::StreamExt;
use owning_ref::OwningHandle;
use rdkafka::client::ClientContext;
use rdkafka::consumer::{BaseConsumer, Consumer, ConsumerContext, MessageStream, Rebalance};
use rdkafka::error::KafkaResult;
use rdkafka::util::TokioRuntime;
use rdkafka::{
    config::{ClientConfig, RDKafkaLogLevel},
    consumer::StreamConsumer,
};
use rdkafka::{Message, TopicPartitionList};
use timely::dataflow::operators::Event;

use crate::domain::{MediawikiRequest, WebRequest};

pub struct WmfConsumerContext;

impl ClientContext for WmfConsumerContext {
    fn stats(&self, statistics: rdkafka::Statistics) {
        tracing::info!("Client stats: {:?}", statistics);
    }
}

impl ConsumerContext for WmfConsumerContext {
    fn pre_rebalance(&self, rebalance: &Rebalance) {
        tracing::info!("Pre rebalance {:?}", rebalance);
    }

    fn post_rebalance(&self, rebalance: &Rebalance) {
        tracing::info!("Post rebalance {:?}", rebalance);
    }

    fn commit_callback(&self, _result: KafkaResult<()>, offsets: &TopicPartitionList) {
        // tracing::info!("Committing offsets: {:?}", offsets);
    }
}

/// creates a generic streaming kafka consumer
pub fn wmf_consumer(group_id: &str) -> StreamConsumer<WmfConsumerContext, TokioRuntime> {
    // pub fn wmf_consumer() -> BaseConsumer<WmfConsumerContext> {
    let brokers = vec![
        "kafka-jumbo1001.eqiad.wmnet:9092",
        "kafka-jumbo1002.eqiad.wmnet:9092",
        "kafka-jumbo1003.eqiad.wmnet:9092",
        "kafka-jumbo1004.eqiad.wmnet:9092",
        "kafka-jumbo1005.eqiad.wmnet:9092",
        "kafka-jumbo1006.eqiad.wmnet:9092",
        "kafka-jumbo1007.eqiad.wmnet:9092",
        "kafka-jumbo1008.eqiad.wmnet:9092",
        "kafka-jumbo1009.eqiad.wmnet:9092",
        ];

    let context = WmfConsumerContext;

    ClientConfig::new()
        .set("group.id", group_id)
        .set("bootstrap.servers", brokers.join(","))
        .set("enable.partition.eof", "false")
        .set("session.timeout.ms", "6000")
        .set("enable.auto.commit", "true")
        // .set("statistics.interval.ms", "5000")
        // .set("auto.offset.reset", "earliest")
        .set("auto.offset.reset", "latest")
        .set_log_level(RDKafkaLogLevel::Debug)
        .create_with_context(context)
        .expect("Consumer creation failed")
}

/// Struct used to own the streaming kafka consumer. Timely dataflow requires a
/// 'static lifetime for a stream and the rdkafka consumer does not support that.
pub struct OwnedConsumerStream {
    /// instant when the stream was started.
    start_instant: Instant,
    /// current instant used to derive the timely dataflow time
    current_instant: Instant,
    /// window duration used to increment the timely dataflow time
    window_duration: Duration,
    /// total number of elements consumed for that stream
    pub total_count: u128,
    /// owned streaming consumer using the owning_ref library
    upstream: OwningHandle<
        Box<StreamConsumer<WmfConsumerContext, TokioRuntime>>,
        Box<MessageStream<'static>>,
    >,
}

impl OwnedConsumerStream {

    // TODO generalize
    // let api_requests = "eqiad.api-gateway.request";
    // let mediawiki_requests = "eqiad.mediawiki.api-request";
    // let page_links_change = "eqiad.mediawiki.page-links-change";
    // let webrequest_text = "webrequest_text";

    pub fn webrequest(group_id: &str, window_duration: Duration) -> Self {

        let topic = "webrequest_text";
        let consumer = wmf_consumer(group_id);

        consumer
            .subscribe(&[topic])
            .expect("No subscribe?");

        let now = Instant::now();
        OwnedConsumerStream {
            start_instant: now.clone(),
            current_instant: now,
            window_duration: window_duration,
            total_count: 0u128,
            upstream: OwningHandle::new_with_fn(Box::new(consumer), |c| {
                let cf = unsafe { &*c };
                Box::new(cf.stream())
            }),
        }
    }
}

// TODO, generalize for other structs
// OwnedConsumerStream should produce a stream of kafka message bytes,
// deserialze the bytes into a struct in a separate step (inside dataflow)
impl futures_util::Stream for OwnedConsumerStream {
    type Item = Event<Option<u64>, WebRequest>;

    fn poll_next(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> std::task::Poll<Option<Self::Item>> {
        if self.current_instant.elapsed() > self.window_duration {
            self.current_instant = Instant::now();
            // let new_time = self.current_instant.duration_since(self.start_instant).as_secs_f32() as u64;
            let new_time = self.current_instant.duration_since(self.start_instant).as_millis() as u64;
            let event: Event<Option<u64>, WebRequest> = Event::Progress(Some(new_time));
            // let event: Event<Option<u64>, MediawikiRequest> = Event::Progress(Some(new_time));
            // println!("{} advancing time", new_time);
            std::task::Poll::Ready(Some(event))
        } else {
            self.upstream.poll_next_unpin(cx).map(|ready| {
                ready.and_then(|result| {
                    match result {
                        Ok(message) => {
                            //TODO add error handling
                            // let time = self.current_instant.duration_since(self.start_instant).as_secs_f32() as u64;
                            let time = self.current_instant.duration_since(self.start_instant).as_millis() as u64;
                            let bytes = message.payload().unwrap();
                            // let req: MediawikiRequest = serde_json::from_slice(bytes).unwrap();
                            let req_option: Option<WebRequest> = serde_json::from_slice(bytes).ok();
                            req_option.map(|req| {
                                let event: Event<Option<u64>, WebRequest> = Event::Message(time, req);
                                self.total_count += 1;
                                event
                            }).or(Some(Event::Progress(Some(time))))
                            // progressing time here is a hack, as emitting None will result in the
                            // consumer to stopped (as the Iterator is done)
                        }
                        Err(err) => {
                            tracing::error!("kafka error {}", err);
                            None
                        }
                    }
                })
            })
        }
    }
}
