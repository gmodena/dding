/// Timely dataflow to compute exponentially decaying counts of pageviews
/// Consumes the webrequest_text kafka event streams
/// Stores output data in key/value database (https://sled.rs/)
/// Starts a web server to serve top k pages per wiki (https://github.com/tokio-rs/axum), updated every 5s

// Start with command: cargo run --release  --bin dding -- -w4
// Starts dd with 4 workers (aka 4 threads consuming from kafka)
// Webserver is on e.g. localhost:3000/top?project=en&family=wikipedia
use serde::{Deserialize, Serialize};
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::{collections::HashMap, time::Duration};
use timely::dataflow::{channels::pact::Pipeline, operators::*};
use tracing_subscriber;

use dding::decay::DecayedValue;
use dding::error::DdingError;
use dding::wmf_kafka::OwnedConsumerStream;

#[tokio::main]
async fn main() -> Result<(), DdingError> {
    tracing_subscriber::fmt::init();

    let tokio_executor = tokio::runtime::Handle::current();

    // TODO make path configurable
    let db_outer = sled::open("sled.db").unwrap();

    let db_async = db_outer.clone();

    // spawn web server to serve data from the kv db
    tokio::spawn(dding::api::start(db_async));

    timely::execute_from_args(std::env::args(), move |worker| {
        let _tokio_guard = tokio_executor.enter();

        let group_id = "rusting3";
        let owned_stream = OwnedConsumerStream::webrequest(group_id, Duration::from_millis(200));
        let native_stream = Box::pin(owned_stream);

        let worker_index = worker.index();
        let worker_peers = worker.peers();

        let db = db_outer.clone();

        let probe = worker.dataflow::<u64, _, _>(move |scope| {
            // webrequests kafka stream
            let reqs = native_stream.to_stream(scope);

            // file based stream of MediawikiRequest
            // let reqs = file_sources::mediawiki_api_request("mediawiki_api_requests.parquet")
            //     .unwrap()
            //     .to_stream(scope)
            //     .delay(|req, _time| {
            //         req.datetime()unix_timestamp() / 60) as u64
            //     });

            // this struct is what is being counted
            #[derive(
                Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize,
            )]
            struct Key {
                project: String,
                project_family: String,
                page_title: String,
            }

            // extract tuples (Key,count)
            let pairs_to_decay = reqs.flat_map(move |req| {
                req.normalized_host()
                    .zip(req.page_title())
                    .and_then(|(nh, pt)| {
                        nh.project.map(|p| {
                            (
                                Key {
                                    project: p,
                                    project_family: nh.project_family,
                                    page_title: pt,
                                },
                                1,
                            )
                        })
                    })
            });

            // exchange the data so that the same key is processed by the same worker
            let exchanged = pairs_to_decay.exchange(move |(key, _)| -> u64 {
                let mut hasher = DefaultHasher::new();
                key.to_owned().hash(&mut hasher);
                hasher.finish().rem_euclid(worker_peers as u64)
            });

            // Generate decayed counts for each incoming (Key,1) tuple
            //   - Emits decayed counts in batches, but only for keys that have changed in that batch
            //   - Internal state is kept in HashMap, one entry per Key
            // Background on operators using frontiers: https://timelydataflow.github.io/timely-dataflow/chapter_2/chapter_2_4.html?highlight=frontier#frontiered-operators
            let decayed_counts =
                exchanged.unary_frontier(Pipeline, "decaying_values", |_default_cap, _info| {
                    // vec used to drain input batch
                    let mut batch_vec = Vec::new();

                    // half life of exponential decay
                    let half_life = (1000 * 60 * 10) as f64;
                    // partial decayed counts per batch
                    let mut batches: HashMap<u64, HashMap<Key, DecayedValue>> = HashMap::new();
                    // full decayed counts, updated and emited as the fronties moes along
                    let mut cumulative: HashMap<Key, DecayedValue> = HashMap::new();

                    // emit every second
                    let mut notificator: FrontierNotificator<u64> = FrontierNotificator::new();
                    let batch_size: u64 = 1000;

                    move |input, output| {
                        while let Some((time, data)) = input.next() {
                            // time of current batch
                            let this_time = time.time().clone() as f64;

                            // compute batch and add notification for end of batch
                            let this_batch_index = time.time() / batch_size;
                            let this_batch = this_batch_index * batch_size;
                            let next_batch = (this_batch_index + 1) * batch_size;
                            let batch_cap = time.delayed(&next_batch);
                            notificator.notify_at(batch_cap);

                            // consume new batch of webrequests
                            data.swap(&mut batch_vec);
                            for req in batch_vec[..].iter() {
                                let (key, val) = req;
                                let new_dv =
                                    DecayedValue::new(val.to_owned() as f64, this_time, half_life);

                                // get or create map for current batch
                                let decay = batches.entry(this_batch).or_insert(HashMap::new());

                                match decay.get_mut(key) {
                                    Some(old_dv) => {
                                        // use AddAssign to mutate the existing decayed value
                                        *old_dv += new_dv;
                                    }
                                    None => {
                                        decay.insert(key.clone(), new_dv);
                                    }
                                }
                            }

                            notificator.for_each(&[input.frontier()], |time, _notificator| {
                                let notified_batch = time.time();

                                let mut times_to_emit: Vec<u64> = batches
                                    .keys()
                                    .copied()
                                    .filter(|t| t < &notified_batch)
                                    .collect();
                                times_to_emit.sort();

                                // Debug println! statements
                                // println!(
                                //     "@time {}. batches to emit: {:?}",
                                //     notified_batch.clone(),
                                //     times_to_emit
                                // );
                                // let batch_mem = batches
                                //     .iter()
                                //     .map(|(k, v)| format!("@{} - {} entries", k.to_owned(), v.len()))
                                //     .collect::<Vec<String>>()
                                //     .join("\n\t");
                                // let cum_mem = format!("cumulative - {} entries", cumulative.len());
                                // println!("\t{}\n\t{}", batch_mem, cum_mem);

                                // the batches ready to be emitted are used to update the
                                // full decayed counts, but only the counts that changed
                                // are emitted
                                for emit_time in times_to_emit {
                                    if let Some(batch_decay) = batches.get(&emit_time) {
                                        // update cumulative counts
                                        for (key, dv) in batch_decay.iter() {
                                            match cumulative.get_mut(key) {
                                                Some(old_dv) => {
                                                    // use AddAssign to mutate the existing decayed value
                                                    *old_dv += *dv;
                                                }
                                                None => {
                                                    cumulative.insert(key.clone(), *dv);
                                                }
                                            }
                                        }

                                        // emit cumulative counts for only keys that are present in the batch to emit
                                        let mut out_vec: Vec<(Key, DecayedValue)> = batch_decay
                                            .keys()
                                            .into_iter()
                                            .flat_map(|k| cumulative.get(k).map(|dv| (k, dv)))
                                            .map(|(k, dv)| (k.to_owned(), dv.to_owned()))
                                            .collect();
                                        output.session(&time).give_vec(&mut out_vec);
                                    }
                                    // remove the emitted batches
                                    batches.remove(&emit_time);
                                }

                                //TODO clear out zeros of cumulative
                            });
                        }
                        // println!(
                        //     "no more elements for decaying value operator at worker {}",
                        //     worker_index
                        // );
                    }
                });

            let top = decayed_counts
                // route the same project to the same worker
                .exchange(move |(key, _)| -> u64 {
                    let mut hasher = DefaultHasher::new();
                    let project = format!("{}.{}", key.project, key.project_family);
                    project.hash(&mut hasher);
                    hasher.finish().rem_euclid(worker_peers as u64)
                })
                .unary_frontier(Pipeline, "top_keys", |_default_cap, _info| {
                    // vec used to drain input batch
                    let mut batch_vec = Vec::new();

                    let top_k_entries = 100;

                    use ordered_float::NotNan;
                    struct TopK {
                        min: (Key, NotNan<f64>),
                        counts: HashMap<Key, NotNan<f64>>,
                    }
                    let mut by_project: HashMap<String, TopK> = HashMap::new();

                    // emit every x seconds
                    let mut notificator: FrontierNotificator<u64> = FrontierNotificator::new();
                    let batch_size: u64 = 5000;

                    move |input, output| {
                        while let Some((time, data)) = input.next() {
                            // compute batch and add notification for end of batch
                            let this_batch_index = time.time() / batch_size;
                            let next_batch = (this_batch_index + 1) * batch_size;
                            let batch_cap = time.delayed(&next_batch);
                            notificator.notify_at(batch_cap);

                            // consume new batch of webrequests
                            data.swap(&mut batch_vec);

                            for (key, val) in batch_vec[..].iter() {
                                let project = format!("{}.{}", key.project, key.project_family);
                                // evaluate the counts as of the current time
                                //TODO remove half_life param
                                let half_life = (1000 * 60 * 10) as f64;
                                let decayed =
                                    val.value_as_of(time.time().clone() as f64, half_life);
                                let not_nan_val = NotNan::new(decayed).unwrap();
                                by_project
                                    .entry(project)
                                    .and_modify(|topk| {
                                        let mut update_min = false;
                                        if topk.counts.len() < top_k_entries {
                                            topk.counts.insert(key.clone(), not_nan_val);
                                            update_min = true;
                                        } else {
                                            // check if observed value is larger than current min
                                            if not_nan_val > topk.min.1 {
                                                // observed val is inserted into TopK
                                                topk.counts.insert(key.clone(), not_nan_val);
                                                // current min is removed
                                                topk.counts.remove(&topk.min.0);
                                                update_min = true;
                                            }
                                        }
                                        if update_min {
                                            // topk has changed, update current min
                                            let new_min =
                                                topk.counts.iter().min_by(|l, r| l.1.cmp(r.1));
                                            topk.min = new_min
                                                .map(|(k, v)| (k.to_owned(), v.to_owned()))
                                                .unwrap();
                                        }
                                    })
                                    .or_insert_with(|| TopK {
                                        min: (key.clone(), not_nan_val),
                                        counts: HashMap::from([(key.clone(), not_nan_val)]),
                                    });
                            }

                            notificator.for_each(&[input.frontier()], |time, _notificator| {
                                let mut out_vec: Vec<(String, Vec<(String, f64)>)> = by_project
                                    .iter()
                                    .map(|(key, topk)| {
                                        //TODO must be a prettier way to do this
                                        let mut topk: Vec<(NotNan<f64>, String)> = topk
                                            .counts
                                            .iter()
                                            .map(|(k, v)| (v.to_owned(), k.page_title.clone()))
                                            .collect();
                                        topk.sort();
                                        topk.reverse();
                                        let topk: Vec<(String, f64)> = topk
                                            .iter()
                                            .map(|(v, k)| (k.to_owned(), v.into_inner()))
                                            .collect();
                                        (key.to_owned(), topk)
                                    })
                                    .collect();

                                println!(
                                    "@time {}. TopKEntry to emit: {:?}",
                                    time.time(),
                                    out_vec.len()
                                );

                                output.session(&time).give_vec(&mut out_vec);
                            });
                        }
                    }
                });

            use std::io::Write;
            let mut file = std::fs::OpenOptions::new()
                .write(true)
                .append(true)
                .create(true)
                .open("data.txt")
                .unwrap();

            let _sunk = top.sink(Pipeline, "write_to_file", move |input| {
                while let Some((time, data)) = input.next() {
                    let mut batch = sled::Batch::default();
                    for datum in data.iter() {
                        if let Err(e) = writeln!(file, "@time {:?}\tdata {:?}", time.time(), datum)
                        {
                            println!("Couldn't write to file: {}", e);
                        }

                        let key = bincode::serialize(datum.0.as_str()).unwrap();
                        let value = bincode::serialize(&datum.1).unwrap();
                        batch.insert(key, value);
                    }

                    db.apply_batch(batch).unwrap();
                }
            });

            top.probe()
        });

        // TODO this doesn't seem necessary, presumingly because the kafka
        // streams never "complete" and there is always more work propagating
        // through the graph
        while probe.less_equal(&27667979) {
            std::thread::sleep(Duration::from_millis(500));
            worker.step();
        }
        probe.with_frontier(|f| println!("last frontier {:?}", f));
        println!("DONE");
    })
    .expect("Timely Dataflow computation failed.");

    Ok(())
}
