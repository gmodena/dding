/// Tool for querying sled.db

use dding::error::DdingError;

#[tokio::main]
async fn main() -> Result<(), DdingError> {

    let db_path = std::env::args().nth(1).expect("sled db path expected");
    println!("Loading DB");
    let db = sled::open(&db_path).unwrap();
    println!("DB loaded");

    let mut buffer = String::new();
    let stdin = std::io::stdin();
    while stdin.read_line(&mut buffer).is_ok() {
        let key = buffer.trim_end();
        println!("querying key {} ", key);

        let res_o = db.get(bincode::serialize(key).unwrap()).unwrap();
        if let Some(res) = res_o {
            let value: Vec<(String, f64)> = bincode::deserialize(&res).unwrap();
            let value = value.iter().enumerate().map(|(i, (pt,pv))| {
                format!("{i}. {pt} - {pv}")
            }).collect::<Vec<_>>().join("\n\t");
            println!("key {}\n\t{}", key, value);
        } else {
            println!("key {} not found", key);
        }
        buffer.clear();

    }
    Ok(())
}