use std::time::Duration;

use rdkafka::consumer::Consumer;

use dding::error::DdingError;


#[tokio::main]
async fn main() -> Result<(), DdingError> {
    tracing_subscriber::fmt::init();
    print_kafka_metadata();
    Ok(())
}

fn print_kafka_metadata() {
    let consumer = dding::wmf_kafka::wmf_consumer("kafkaing");

    // let topic = Some("eqiad.mediawiki.api-request");
    let topic = Some("webrequest_text");
    let fetch_offsets = true;
    let timeout = Duration::from_secs(5);

    let x = consumer.fetch_group_list(None, timeout).unwrap();
    for g in x.groups().iter() {
        let members: String = g
            .members()
            .iter()
            .map(|m| {
                format!(
                    "host: {} id: {} metadata: {:?}",
                    m.client_host(),
                    m.client_id(),
                    String::from_utf8_lossy(&m.metadata().unwrap())
                )
            })
            .collect::<Vec<String>>()
            .join("\n\t");
        println!("name {:?}", g.name());
        println!("protocol {:?}", g.protocol());
        println!("state {:?}", g.state());
        println!("members {}", members);
    }

    let metadata = consumer
        .fetch_metadata(topic, timeout)
        .expect("Failed to fetch metadata");

    let mut message_count = 0;

    println!("Cluster information:");
    println!("  Broker count: {}", metadata.brokers().len());
    println!("  Topics count: {}", metadata.topics().len());
    println!("  Metadata broker name: {}", metadata.orig_broker_name());
    println!("  Metadata broker id: {}\n", metadata.orig_broker_id());

    println!("Brokers:");
    for broker in metadata.brokers() {
        println!(
            "  Id: {}  Host: {}:{}  ",
            broker.id(),
            broker.host(),
            broker.port()
        );
    }

    println!("\nTopics:");
    for topic in metadata.topics() {
        println!("  Topic: {}  Err: {:?}", topic.name(), topic.error());
        for partition in topic.partitions() {
            println!(
                "     Partition: {}  Leader: {}  Replicas: {:?}  ISR: {:?}  Err: {:?}",
                partition.id(),
                partition.leader(),
                partition.replicas(),
                partition.isr(),
                partition.error()
            );
            if fetch_offsets {
                let (low, high) = consumer
                    .fetch_watermarks(topic.name(), partition.id(), Duration::from_secs(1))
                    .unwrap_or((-1, -1));
                println!(
                    "       Low watermark: {}  High watermark: {} (difference: {})",
                    low,
                    high,
                    high - low
                );
                message_count += high - low;
            }
        }
        if fetch_offsets {
            println!("     Total message count: {}", message_count);
        }
    }
}
