/// Tool to play around with decaying counts
/// TODO, these should be unit tests
use dding::{error::DdingError, decay::DecayedValue};

#[tokio::main]
async fn main() -> Result<(), DdingError> {
    tracing_subscriber::fmt::init();

    let half_life = (1000 * 60 * 10) as f64;
    // let half_life = (10) as f64;
    let mut dv = DecayedValue::new(100f64, 0f64, half_life);


    for i in ((1000*60)..(1000 * 60 * 10)).step_by(1000*60) {
        // println!("{}",dv.average(half_life));
        // println!("{}",dv.average_interval(0f64, 10f64, half_life));
        if i == 300000 {
            dv += DecayedValue::new(100f64, i as f64, half_life);
        }

        dv.update_as_of(i as f64, half_life);
        println!("@{} - {:?}",i, dv);
        println!("average {} , {}",dv.average(half_life),dv.average_interval((i-1000*60) as f64, i as f64, half_life));
    }

    // println!("{:?}",dv);

    // let dv1 = decaying_values::DecayedValue::new(100f64, 5f64, half_life);

    // dv += dv1;

    // println!("{:?}",dv);
    // println!("{}",dv.average(half_life));
    // println!("{}",dv.average_interval(0f64, 10f64, half_life));


    Ok(())
}