/// exponentially decaying value as semigroup
/// based on https://github.com/twitter/algebird/blob/develop/algebird-core/src/main/scala/com/twitter/algebird/DecayedValue.scala#L31
use std::ops::AddAssign;
use serde::{Deserialize, Serialize};

/// do we get much from saving the half_life param from being in the struct?
/// has to be passed around everywhere which is annoying/confusing
/// const generic maybe if fixed time interval by default, caveat only works with integers
/// zero cost
// pub struct DecayedValue1<const HL: usize> {
//     pub value: f64,
//     pub time: f64,
// }
// impl<const HL: usize> DecayedValue1<HL>{
//     const HALF_LIFE: f64 = HL as f64;

//     fn scaled_time(&mut self) -> f64 {
//         self.time * 2f64.ln() / Self::HALF_LIFE
//     }
// }

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub struct DecayedValue {
    pub value: f64,
    pub scaled_time: f64,
}

impl DecayedValue {
    pub fn new(value: f64, time: f64, half_life: f64) -> Self {
        let scaled_time = time * 2f64.ln() / half_life;
        DecayedValue {
            value: value,
            scaled_time: scaled_time,
        }
    }

    pub fn is_zero(&self) -> bool {
        self.value < EPSILON
    }

    /// update DecayedValue to new time
    pub fn update_as_of(&mut self, time: f64, half_life: f64) {
        *self += DecayedValue::new(0f64, time, half_life);
    }

    // compte value as of `time`
    pub fn value_as_of(&self, time: f64, half_life: f64) -> f64 {
        let mut dv = DecayedValue::new(0f64, time, half_life);
        dv += *self;
        dv.value
    }

    pub fn average(&self, half_life: f64) -> f64 {
        let normalization = half_life / 2f64.ln();
        self.value / normalization
    }

    pub fn average_interval(&self, start_time: f64, end_time: f64, half_life: f64) -> f64 {
        assert!(start_time < end_time, "start after end");
        let mut dv = self.clone();
        dv += DecayedValue::new(0f64, end_time, half_life);
        let time_delta = start_time - end_time;
        let normalization = half_life * (1f64 - 2f64.powf(time_delta / half_life)) / 2f64.ln();
        dv.value / normalization
    }
}

/// semigroup for decayed value. plain function for now
/// could be a core::ops::AddAssign where is mutated
/// could be a differential_dataflow::difference::Monoid
/// espsilon pull small values to 0
const EPSILON: f64 = 0.001f64;
impl AddAssign for DecayedValue {
    fn add_assign(&mut self, rhs: Self) {
        if self.scaled_time < rhs.scaled_time {
            //  self is older,
            self.value = rhs.value + (self.scaled_time - rhs.scaled_time).exp() * self.value;
            self.scaled_time = rhs.scaled_time;
        } else {
            //  right is older
            self.value = self.value + (rhs.scaled_time - self.scaled_time).exp() * rhs.value;
        }
    }
}

// const ZERO: DecayedValue = DecayedValue {
//     value: 0f64,
//     scaled_time: f64::NEG_INFINITY,
// };
// pub fn plus(left: DecayedValue, right: DecayedValue) -> DecayedValue {
//     if left.scaled_time < right.scaled_time {
//         // left is older:
//         scale(right, left)
//     } else {
//         // right is older:
//         scale(left, right)
//     }
// }
// fn scale(new_dv: DecayedValue, old_dv: DecayedValue) -> DecayedValue {
//     let new_value =
//         new_dv.value + (old_dv.scaled_time - new_dv.scaled_time).exp() * old_dv.value;
//     if new_value.abs() > EPSILON {
//         DecayedValue {
//             value: new_value,
//             scaled_time: new_dv.scaled_time,
//         }
//     } else {
//         ZERO
//     }
// }
