/// Domain representations and implementation
///
use parquet::record::Row;
use serde::{Deserialize, Serialize};
use time::{PrimitiveDateTime, format_description::well_known::Iso8601, OffsetDateTime};

use crate::error::DdingError;

// https://schema.wikimedia.org/#!//primary/jsonschema/mediawiki
// https://gerrit.wikimedia.org/r/plugins/gitiles/schemas/event/primary/+/refs/heads/master
// todo script that copies the schemas

// derive structs for JsonSchema specifications
schemafy::schemafy!(root: MediawikiRequest "schemas/mediawiki_api_request.json");
schemafy::schemafy!(root: PageLinkChange "schemas/page_link_change.json");

impl MediawikiRequest {
    /// Attempt to parse a MediawikiRequest from a json byte array, extracted
    /// from a kafka message
    pub fn from_json(bytes: &[u8]) -> Result<MediawikiRequest, DdingError> {
        serde_json::from_slice::<MediawikiRequest>(bytes).map_err(|e| e.into())
    }

    /// Attempt to parse a MediawikiRequest from a parquet row
    /// The struct is derrived using the JsonSchema that is used
    /// for kafka events. These events are also stored on hdfs as
    /// parquet files. This function converts a parquet row into a
    /// json Value, which is then parsed into the struct
    pub fn from_parquet_row(row: Row) -> Result<MediawikiRequest, DdingError> {
        let mut val = row.to_json_value();
        // insert the $schema key that is required by the JsonSchema
        let object = val.as_object_mut().unwrap();
        object.insert(
            "$schema".to_owned(),
            serde_json::Value::String("https://json-schema.org/draft-07/schema#".to_owned()),
        );
        serde_json::from_value::<MediawikiRequest>(val).map_err(|e| e.into())
    }
}

/// struct based on the avro source for wikitext history
/// https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Content/Mediawiki_wikitext_history
#[derive(Debug, Deserialize)]
pub struct WikitextHistory {
    wiki_db: String,
    page_id: i32,
    page_namespace: i32,
    page_title: String,
    page_redirect_title: String,
    page_restrictions: Vec<String>,
    user_id: i32,
    user_text: String,
    revision_id: i32,
    revision_parent_id: i32,
    revision_timestamp: String,
    revision_minor_edit: bool,
    revision_comment: String,
    revision_text_bytes: i32,
    revision_text_sha1: String,
    revision_text: String,
    revision_content_model: String,
    revision_content_format: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct WebRequest {
    hostname: String,                // "cp4036.ulsfo.wmnet"
    sequence: u64,                   // 9716003321
    pub dt: String,                      // "2022-09-07T02:29:58Z"
    time_firstbyte: f32,             // 0.000211
    ip: String,                      // "49.227.71.45"
    cache_status: String,            // "hit-front"
    http_status: String,             // "200"
    response_size: u32,              // 1848
    http_method: String,             // "GET"
    uri_host: String,                // "en.m.wikipedia.org"
    uri_path: String,                // "/w/load.php"
    uri_query: String, // "?lang=en&modules=ext.relatedArticles.readMore&skin=minerva&version=1csry"
    content_type: String, // "text/javascript; charset=utf-8"
    referer: String,   // "https: String//en.m.wikipedia.org/wiki/Magic_Mike_XXL"
    user_agent: String, // "Mozilla/5.0 (iPhone; CPU iPhone OS 15_6_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6.1 Mobile/15E148 Safari/604.1"
    accept_language: String, // "en-au,en;q=0.9"
    x_analytics: String, // "WMF-Last-Access=07-Sep-2022;WMF-Last-Access-Global=07-Sep-2022;https=1;client_port=49275"
    range: String,       // "-"
    x_cache: String,     // "cp4036 hit, cp4036 hit/112112"
    accept: String,      // "*/*"
    backend: String,     // "ATS/8.0.8"
    tls: String, // "vers=TLSv1.3;keyx=UNKNOWN;auth=ECDSA;ciph=AES-256-GCM-SHA384;prot=h2;sess=new"
    ch_ua: String, // "-"
    ch_ua_arch: String, // "-"
    ch_ua_bitness: String, // "-"
    ch_ua_full_version_list: String, // "-"
    ch_ua_mobile: String, // "-"
    ch_ua_model: String, // "-"
    ch_ua_platform: String, // "-"
    ch_ua_platform_version: String, // "-"
}

static URI_PATH_API: &'static str = "api.php";

static URI_QUALIFIERS: [&'static str; 6] = ["m", "mobile", "wap", "zero", "www", "download"];
static WMF_DOMAINS: [&'static str; 17] = [
    "wikimedia.org",
    "wikibooks.org",
    "wikinews.org",
    "wikipedia.org",
    "wikiquote.org",
    "wikisource.org",
    "wiktionary.org",
    "wikiversity.org",
    "wikivoyage.org",
    "wikidata.org",
    "mediawiki.org",
    "wikimediafoundation.org",
    "wikiworkshop.org",
    "wmfusercontent.org",
    "wmflabs.org",
    "wmcloud.org",
    "toolforge.org"];

static SUCCESS_HTTP_STATUSES: [&'static str; 2] = ["200", "304"];
static REDIRECT_HTTP_STATUSES: [&'static str; 3] = ["301", "302", "307"];

static TEXT_HTML_CONTENT_TYPES : [&'static str; 5] = [
    "text/html",
    "text/html; charset=iso-8859-1",
    "text/html; charset=ISO-8859-1",
    "text/html; charset=utf-8",
    "text/html; charset=UTF-8"];

static PAGE_TITLE_PATHS : [&'static str; 5] = [
    "/wiki/",
    "/w/index.php/",
    "/api/rest_v1/page/mobile-sections-lead/",
    "/api/rest_v1/page/mobile-sections/",
    "/api/rest_v1/page/mobile-html/"];

#[derive(Clone, Debug)]
pub struct NormalizedHost {
    pub project: Option<String>,
    pub qualifiers: Vec<String>,
    pub project_family: String,
    pub tld: String,
}

/// extractor functions based on
/// https://github.com/wikimedia/analytics-refinery-source/blob/8154872ac55a7059dacb39ba14566ea388b3720b/refinery-core/src/main/java/org/wikimedia/analytics/refinery/core/Webrequest.java#L32
///
/// X-Analytics: https://wikitech.wikimedia.org/wiki/X-Analytics
impl WebRequest {
    /// parse uri_host
    /// "en.m.wikipedia.org"
    /// TODO edge cases schmedge cases
    pub fn normalized_host(&self) -> Option<NormalizedHost> {
        let parts: Vec<&str> = self.uri_host.split(".").collect();
        match parts[..] {
            [project_family, tld] => {
                Some(NormalizedHost {
                    project: None,
                    qualifiers: vec![],
                    project_family: project_family.to_string(),
                    tld: tld.to_string(),
                })
            },
            [project, project_family, tld] => {
                Some(NormalizedHost {
                    project: Some(project.to_string()),
                    qualifiers: vec![],
                    project_family: project_family.to_string(),
                    tld: tld.to_string(),
                })
            },
            [project, qualifier, project_family, tld] => {
                if !URI_QUALIFIERS.contains(&project) && URI_QUALIFIERS.contains(&qualifier) {
                    Some(NormalizedHost {
                        project: Some(project.to_string()),
                        qualifiers: vec![qualifier.to_string()],
                        project_family: project_family.to_string(),
                        tld: tld.to_string(),
                    })
                } else {
                    //TODO
                    None
                }
            },
            _ => None,
        }
    }

    /// poor man's pageview classifier
    /// https://github.com/wikimedia/analytics-refinery-source/blob/8154872ac55a7059dacb39ba14566ea388b3720b/refinery-core/src/main/java/org/wikimedia/analytics/refinery/core/PageviewDefinition.java#L267
    /// TODO edge cases schmedge cases
    pub fn is_page_view(&self) -> bool {
        let wmf_host = WMF_DOMAINS.iter().any(|domain| self.uri_host.contains(domain));
        let success = SUCCESS_HTTP_STATUSES.contains(&self.http_status.as_str());

        let app_agent = self.user_agent.contains("WikipediaApp");

        let web_pageview = {
            let html_content_type = TEXT_HTML_CONTENT_TYPES.contains(&self.content_type.as_str());
            let api = self.uri_path.contains(URI_PATH_API);
            let edit = self.uri_query.contains("action=edit");
            !app_agent && html_content_type && !api && !edit
        };

        let app_page_view = {
            let tagged = self.x_analytics.contains("pageview=1");
            app_agent && tagged
        };

        wmf_host && success && (web_pageview || app_page_view)
    }

    /// extract the page title from uri_path / uri_query
    /// order of preference:
    ///   1. "title" query param from uri_query
    ///   2. extracted from uri_path
    ///   3. "page" query param from uri_query
    pub fn page_title(&self) -> Option<String> {

        if !self.is_page_view() {
            return None
        }

        let mut page_query_param: Option<&str> = None;
        for pair in self.uri_query.split("&") {
            match pair.split("=").collect::<Vec<_>>()[..] {
                [key, value] => {
                    // if there is a title param, use it
                    if key == "title" {
                        // if there is a title param, return since #1 choice
                        return Some(value.to_string());
                    } else if key == "page" {
                        page_query_param = Some(value)
                    }
                },
                _ => (),
            }
        }
        // General case of page title in path
        let path_page_title = PAGE_TITLE_PATHS.iter()
            .map(|path|{
                if self.uri_path.contains(path) {
                    Some(&self.uri_path[path.len()..])
                } else {
                    None
                }
            })
            .find(|o| o.is_some())
            .flatten();

        path_page_title
            .or(page_query_param)
            .and_then(|s| {
                //TODO wiki page name encoding is a science, https://en.wikipedia.org/wiki/Wikipedia:Page_name
                //simply url decoding is too simple to be correct...
                //TODO add error handling
                urlencoding::decode(s).ok()
            })
            .map(|s| s.to_string())


    }

    //
    pub fn datetime(&self) -> OffsetDateTime {
        let date = PrimitiveDateTime::parse(&self.dt, &Iso8601::DEFAULT).unwrap();
        // (date.assume_utc().unix_timestamp() *1000) as u64
        date.assume_utc()
    }


}
