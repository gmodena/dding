/// file based sources
///
use apache_avro::from_value;
use apache_avro::Reader;
use parquet::file::reader::SerializedFileReader;

use std::convert::TryFrom;
use std::fs::File;

use crate::domain::MediawikiRequest;
use crate::domain::WikitextHistory;
use crate::error::DdingError;

pub fn wikitext_history(
    file_name: &str,
) -> Result<impl Iterator<Item = WikitextHistory>, DdingError> {
    let file = File::open(file_name)?;
    let reader = Reader::new(file)?;

    // schema is not set for wmf data?
    // let schema = reader.reader_schema().unwrap();

    // filter out deserialization error
    let iter = reader.into_iter().flat_map(|res| {
        res.map_err(|e| DdingError::AvroError(e))
            .and_then(|v| from_value::<WikitextHistory>(&v).map_err(|e| e.into()))
            .map_err(|err| {
                tracing::warn!("Discarding avro value; error deserializing {:?}. ", err)
            })
    });
    Ok(iter)
}

pub fn mediawiki_api_request(file_name: &str) -> Result<impl Iterator<Item = MediawikiRequest>, DdingError> {
    let iter = SerializedFileReader::try_from(file_name).map(|r| r.into_iter())?;

    // filter out deserialization error
    let reqs_iter = iter.flat_map(|row| {
        MediawikiRequest::from_parquet_row(row).map_err(|err| {
            tracing::warn!(
                "Discarding parquet row; error deserializing {:?}. ",
                err
            )
        })
    });

    Ok(reqs_iter)
}

// pub fn parquet(file_name: &str) -> Result<(), DdingError> {
//     let file = File::open(file_name)?;

//     let mut arrow_reader = ParquetFileArrowReader::try_new(file)?;

//     let schema = arrow_reader.get_schema()?;
//     for f in schema.fields {
//         println!("{}",f)
//     }

//     let batch_size = 1;

//     // for now, all columns are read
//     // let mut record_batch_reader = arrow_reader.get_record_reader(batch_size)?;

//     // TODO, if desired/needed we could project the desired columns to read
//     let mask = ProjectionMask::roots(arrow_reader.parquet_schema(), [2,3]);
//     let mut record_batch_reader = arrow_reader
//         .get_record_reader_by_columns(mask, batch_size)?;

//     // let schema = record_batch_reader()?;
//     // for f in schema.fields {
//     //     println!("{}",f)
//     // }

//     for maybe_record_batch in record_batch_reader.take(1) {
//         let record_batch = maybe_record_batch.unwrap();

//         let _ = event_structs::MediawikiRequest::from_record_batch(&record_batch);

//         // let x = record_batch.columns();
//         // println!("{:?}", x);
//         // let y = x.iter().map(|r| {
//         //     println!("{:?}",r);
//         // }).take(1).collect::<Vec<_>>();
//     }
//     Ok(())
// }
