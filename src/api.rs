use axum::{
    routing::{get, post},
    http::StatusCode,
    response::IntoResponse,
    Json, Router, extract::{Extension, Query},
};
use serde::{Deserialize, Serialize};
use std::{net::SocketAddr, sync::Arc};

pub async fn start(sled_db: sled::Db) {

    let shared_state = Arc::new(sled_db);

    // build our application with a route
    let app = Router::new()
        // `GET /` goes to `root`
        .route("/", get(root))
        // `POST /users` goes to `create_user`
        .route("/top", get(topk_per_project))
        .layer(Extension(shared_state));


    // run our app with hyper
    // `axum::Server` is a re-export of `hyper::Server`
    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

// basic handler that responds with a static string
async fn root() -> &'static str {
    "DDing API"
}


#[derive(Deserialize, Serialize)]
struct PerProject {
    project: String,
    family: String,
}

#[derive(Serialize)]
struct TopKPerProject {
    project_info: PerProject,
    topk: Vec<(String, f64)>
}

async fn topk_per_project(
    per_project: Query<PerProject>,
    Extension(db): Extension<Arc<sled::Db>>) -> impl IntoResponse {
    let per_project: PerProject = per_project.0;
    let key = format!("{}.{}", per_project.project, per_project.family);
    tracing::info!("XXX {}", key);
    match db.get(bincode::serialize(&key).unwrap()).unwrap() {
        Some(bytes) => {
            let value: Vec<(String, f64)> = bincode::deserialize(&bytes).unwrap();
            let json = Json(TopKPerProject { project_info: per_project, topk: value });
            (StatusCode::OK, json)
        },
        None => {
            let json = Json(TopKPerProject { project_info: per_project, topk: vec![] });
            (StatusCode::OK, json)
        },
    }
}
