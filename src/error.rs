/// error traits

#[derive(Debug)]
pub enum DdingError {
    Custom(String),
    IoError(std::io::Error),
    KafkaError(rdkafka::error::KafkaError),
    AvroError(apache_avro::Error),
    ParquetError(parquet::errors::ParquetError),
    ParseError(serde_json::Error),
}

impl From<std::io::Error> for DdingError {
    fn from(err: std::io::Error) -> Self {
        DdingError::IoError(err)
    }
}

impl From<rdkafka::error::KafkaError> for DdingError {
    fn from(err: rdkafka::error::KafkaError) -> Self {
        DdingError::KafkaError(err)
    }
}

impl From<apache_avro::Error> for DdingError {
    fn from(err: apache_avro::Error) -> Self {
        DdingError::AvroError(err)
    }
}

impl From<parquet::errors::ParquetError> for DdingError {
    fn from(err: parquet::errors::ParquetError) -> Self {
        DdingError::ParquetError(err)
    }
}

impl From<serde_json::Error> for DdingError {
    fn from(err: serde_json::Error) -> Self {
        DdingError::ParseError(err)
    }
}
